using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);

        Task AddCustomerAsync(Customer customer);

        void AddCustomers(IEnumerable<Customer> customers);

        Task AddCustomersAsync(IEnumerable<Customer> customers);

        void SaveChanges();

        Task SaveChangesAsync();
    }
}