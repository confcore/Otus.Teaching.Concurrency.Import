using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        private const int DefaultWaitTime = 10_000;

        public void LoadData()
        {
            Console.WriteLine("Loading data...");
            Thread.Sleep(DefaultWaitTime);
            Console.WriteLine("Loaded data...");
        }

        public async Task LoadDataAsync()
        {
            Console.WriteLine("Loading data...");
            await Task.Delay(DefaultWaitTime);
            Console.WriteLine("Loaded data...");
        }
    }
}