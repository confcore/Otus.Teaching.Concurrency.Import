﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.Core.Extensions
{
    public static class CustomersExtensions
    {
        public static IEnumerable<IEnumerable<Customer>> ChunkById(
            this IEnumerable<Customer> source,
            int chunkSize)
        {
            if (chunkSize < 1)
            {
                throw new ArgumentException("Chunk size must be greater than 0.");
            }

            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static IEnumerable<IEnumerable<Customer>> Chunk(
            this IEnumerable<Customer> source,
            int chunkSize)
        {
            if (chunkSize < 1)
            {
                throw new ArgumentException("Chunk size must be greater than 0.");
            }

            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
            {
                yield return Take(enumerator.Current, enumerator, chunkSize);
            }
        }

        private static IEnumerable<Customer> Take(Customer head, IEnumerator<Customer> tail, int size)
        {
            while (true)
            {
                yield return head;
                if (--size == 0)
                {
                    break;
                }

                if (tail.MoveNext())
                {
                    head = tail.Current;
                }
                else
                {
                    break;
                }
            }
        }
    }
}
