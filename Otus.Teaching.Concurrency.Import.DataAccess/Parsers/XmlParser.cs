﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public XmlParser(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }

        public List<Customer> Parse()
        {
            using var stream = new StreamReader(FileName);
            var customersList = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(stream);
            return customersList.Customers;
        }
    }
}