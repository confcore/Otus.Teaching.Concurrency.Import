﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Extensions;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Handlers
{
    public class CustomerLoaderHandler
    {
        public const int ChunkSize = 5_000;
        private readonly IEnumerable<Customer> _customers;

        public CustomerLoaderHandler(IEnumerable<Customer> customers)
        {
            _customers = customers;
        }

        public void Handle()
        {
            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} started loading {_customers.Count()} items...");

            var chunks = _customers.Chunk(ChunkSize);
            foreach (var chunk in chunks)
            {
                using var repository = new CustomerRepository(new CustomerDataContext());
                repository.AddCustomers(chunk);
                TrySaveChanges(repository);
            }

            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} finished loading {_customers.Count()} items.");
        }

        public async Task HandleAsync()
        {
            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} started loading async {_customers.Count()} items...");

            var chunks = _customers.Chunk(ChunkSize);
            foreach (var chunk in chunks)
            {
                using var repository = new CustomerRepository(new CustomerDataContext());
                await repository.AddCustomersAsync(chunk);
                await TrySaveChangesAsync(repository);
            }

            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} finished loading async {_customers.Count()} items.");
        }

        private void TrySaveChanges(CustomerRepository repository)
        {
            int tryCounter = 0;
            do
            {
                try
                {
                    repository.SaveChanges();
                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);
                    tryCounter++;
                }
            }
            while (tryCounter < 30);
        }

        private async Task TrySaveChangesAsync(CustomerRepository repository)
        {
            int tryCounter = 0;
            do
            {
                try
                {
                    await repository.SaveChangesAsync();
                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);
                    tryCounter++;
                }
            }
            while (tryCounter < 30);
        }
    }
}
