﻿using System;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader.Helpers
{
    public static class ProcessHelper
    {
        public static int StartProcess(string fileName, params string[] arguments)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("File name cannot be null or whitespace string.");
            }

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = fileName,
                    Arguments = string.Join(' ', arguments),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            process.Start();
            while (!process.StandardOutput.EndOfStream)
            {
                var line = process.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }

            process.WaitForExit();
            return process.ExitCode;
        }
    }
}
