using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Data
{
    public interface IDataGenerator
    {
        void Generate();

        Task GenerateAsync();
    }
}