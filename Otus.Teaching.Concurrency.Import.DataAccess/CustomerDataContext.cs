﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class CustomerDataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Customers.sqlite");

            // For testing with PostgreSQL.
            //optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Customers;Username=postgres;Password=admin");
        }
    }
}
