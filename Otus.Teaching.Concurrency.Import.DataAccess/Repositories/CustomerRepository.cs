using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public sealed class CustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly CustomerDataContext _dataContext;
        private bool disposed = false;

        public CustomerRepository(CustomerDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void AddCustomer(Customer customer)
        {
            _dataContext.Customers.Add(customer);
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _dataContext.Customers.AddAsync(customer);
        }

        public void AddCustomers(IEnumerable<Customer> customers)
        {
            _dataContext.Customers.AddRange(customers);
        }

        public async Task AddCustomersAsync(IEnumerable<Customer> customers)
        {
            await _dataContext.Customers.AddRangeAsync(customers);
        }

        public void SaveChanges()
        {
            _dataContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dataContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                _dataContext?.Dispose();
            }

            disposed = true;
        }
    }
}