﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Helpers;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static int _dataCount = 1_000_000;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static string _generatorFilePath = null;

        static async Task Main(string[] args)
        {
            ParseArgs(args);

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (string.IsNullOrWhiteSpace(_generatorFilePath))
            {
                await GenerateCustomersDataFileAsync();
            }
            else
            {
                string filePathWithoutExtension = GetFullFilePathWithoutExtension(_dataFilePath);

                int generatorResult = ProcessHelper.StartProcess(_generatorFilePath, filePathWithoutExtension, _dataCount.ToString());
                Console.WriteLine($"Generator process exited with code {generatorResult}.");
            }

            var xmlParser = new XmlParser(_dataFilePath);
            var customers = xmlParser.Parse();

            InitializeDb();

            int processorCount = Environment.ProcessorCount;
            var loader = new CustomerDataLoader(customers, processorCount);
            await loader.LoadDataAsync();
        }

        private static void ParseArgs(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                return;
            }

            if (args.Length == 1)
            {
                if (args[0].EndsWith(".exe"))
                {
                    _generatorFilePath = args[0];
                }
                else
                {
                    _dataFilePath = args[0];
                }
            }
            else
            {
                _dataFilePath = args[0];
                _generatorFilePath = args[1];
            }
        }

        static async Task GenerateCustomersDataFileAsync()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, _dataCount);
            await xmlGenerator.GenerateAsync();
        }

        static void InitializeDb()
        {
            using var dataContext = new CustomerDataContext();
            new DbInitializer(dataContext).Initialize();
        }

        static string GetFullFilePathWithoutExtension(string filePath)
        {
            return Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath));
        }
    }
}