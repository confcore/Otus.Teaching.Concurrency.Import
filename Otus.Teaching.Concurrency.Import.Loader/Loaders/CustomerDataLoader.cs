﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Extensions;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class CustomerDataLoader : IDataLoader
    {
        private readonly List<Customer> _customers;
        private readonly int _threadCount;

        public CustomerDataLoader(List<Customer> customers, int threadCount)
        {
            _customers = customers;
            _threadCount = threadCount;
        }

        public void LoadData()
        {
            var customersChunks = ChunkCustomers();

            Console.WriteLine("Started loading data...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            List<Thread> loaderThreads = new List<Thread>();
            foreach (var chunk in customersChunks)
            {
                loaderThreads.Add(CreateHandlerThread(chunk));
            }

            loaderThreads.ForEach(x => x.Start());
            loaderThreads.ForEach(x => x.Join());

            stopwatch.Stop();
            Console.WriteLine($"Loaded data in {stopwatch.Elapsed}...");
        }

        public async Task LoadDataAsync()
        {
            var customersChunks = ChunkCustomers();

            Console.WriteLine("Started loading data async...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            List<Task> loaderTasks = new List<Task>();
            foreach (var chunk in customersChunks)
            {
                var loaderTask = CreateHandlerTask(chunk);
                loaderTasks.Add(loaderTask);
            }

            await Task.WhenAll(loaderTasks);

            stopwatch.Stop();
            Console.WriteLine($"Loaded data async in {stopwatch.Elapsed}...");
        }

        private IEnumerable<IEnumerable<Customer>> ChunkCustomers()
        {
            int chunkSize = (_customers.Count + _threadCount - 1) / _threadCount;
            return _customers.ChunkById(chunkSize); ;
        }

        private Thread CreateHandlerThread(IEnumerable<Customer> data)
        {
            var handler = new CustomerLoaderHandler(data);
            return new Thread(handler.Handle);
        }

        private Task CreateHandlerTask(IEnumerable<Customer> data)
        {
            var handler = new CustomerLoaderHandler(data);
            return Task.Run(() => handler.HandleAsync());
        }
    }
}
