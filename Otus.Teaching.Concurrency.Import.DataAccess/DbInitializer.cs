﻿using System;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DbInitializer
    {
        private readonly CustomerDataContext _dataContext;

        public DbInitializer(CustomerDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}
